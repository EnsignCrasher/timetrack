#!/bin/sh

set -e
set -u

readonly script_path="$(cd "$(dirname "${0}")" && pwd)"
readonly command="${1?please provide a command (add/substract)}"
readonly time="${2?Please provide a time in seconds}"

if ! readonly current_time=$(sed -n 1p "${script_path}/current"); then
	printf "No track in progress\n"
	exit 1
fi

if test "${command}" = "add"; then
	readonly new_time="$((${current_time} - ${time}))"
elif test "${command}" = "substract"; then
	new_time="$((${current_time} + ${time}))"
	if ! test "${new_time}" -lt "$(date +%s)"; then
		new_time="$(date +%s)"
	fi
	readonly new_time
fi

sed -i'' '1 s/.*/'"${new_time}"'/g' "${script_path}/current"
